import React from 'react';
import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger = (props) => {
    let keys= Object.keys(props.ingredients)
    let value= keys.map(igKey =>{
        return [...Array(props.ingredients[igKey])]
        .map((_, i) =>{
         return <BurgerIngredient key={igKey + i} type={igKey} />
     });
 })
    let transformedIngredients =value.reduce((arr,el) =>{
                return arr.concat(el)
            }, []);
        if (transformedIngredients.length === 0){
            transformedIngredients = <p>Start adding ingredients!</p>;
        }
    return(
        <div className={classes.burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger;